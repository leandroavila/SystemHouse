<?php
  include("utils/conexao.php");
  include("utils/banco.php");
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <?php include("includes/includeHead.php"); ?>
</head>

<body>
  <!--STYLE PADDING PARA FORCAR A REMOCAO DO ESPACAMENTO NO CONTAINER FLUIDO-->
  <div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">

    <?php include("includes/header.php"); ?>

    <div class="row margin-fixada">
      <div class="col-sm-12">
        <!-- CONTEUDO DA PAGINA -->
      </div>
    </div>

    <?php include("includes/footer.php"); ?>

  </div>

  <!-- ARQUIVOS JAVASCRIPT -->
  <?php include("includes/includeJavascript.php"); ?>

</body>
</html>
