<?php
  include("utils/conexao.php");
  include("utils/banco.php");
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <?php include("includes/includeHead.php"); ?>
</head>

<body>
<!--STYLE PADDING PARA FORCAR A REMOCAO DO ESPACAMENTO NO CONTAINER FLUIDO-->
<div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">

    <?php include("includes/header.php"); ?>

    <div class="row margin-fixada">

        <div class="col-md-3"></div>

        <!--div class="col-md-3">
            <h5 class="text-jusitify text-primary">Localização System House</h5><hr>
            <a class="text-justify">A System House está localizada na cidade de Araguari - MG.</a>
        </div-->

        <div class="col-md-6 card-header">

          <?php
            if(!empty($_GET["status"])) {
              echo
              '<p class="alert alert-success efeito-fade">'.
              '  E-mail enviado com sucesso!'.
              '</p>';
            }
          ?>

          <h5 class="text-justify text-primary">Contato System House</h5><hr>
          <p class="text-justify">A sua opinião é importante para a System House</p>

          <div class="form-group-sm">
              <form method="post" action="utils/envioDeEmail.php" name="form_contato" method="post">
                  <div class="form-group">
                      <input type="text" class="form-control" id="idNome" placeholder="Nome" name="nome" required="">
                  </div>

                  <div class="form-group">
                      <input type="email" class="form-control" id="idEmail" placeholder="E-mail" name="email"
                             required>
                  </div>

                  <div class="form-group">
                      <input type="tel" class="form-control" id="idTelefone" placeholder="Telefone" name="telefone"
                             required="">
                  </div>

                  <div class="form-group">
                          <input type="text" class="form-control" id="idAssunto" placeholder="Assunto" name="assunto"
                             required>
                  </div>

                  <div class="form-group">
                      <textarea class="form-control" id="textarea_mensagem" placeholder="Digite aqui sua mensagem"
                                name="mensagem" required=""></textarea>
                  </div>

                  <button type="reset" class="btn btn-primary">Limpar</button>
                  <button type="submit" class="btn btn-primary">Enviar</button>
              </form>
          </div>
      </div>

      <div class="col-md-3">

      </div>
    </div>

    <?php include("includes/footer.php"); ?>

</div>



<!-- ARQUIVOS JAVASCRIPT -->
<?php include("includes/includeJavascript.php"); ?>

</body>
</html>
