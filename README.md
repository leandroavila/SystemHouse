# SITE PRINCIPAL SYSTEM HOUSE  

## Descrição
>Este projeto provê o conteúdo do site principal da empresa **System House** e aborda alguns conteúdos como
- Sobre a Empresa
- Produtos
- Serviços
- Clientes
- Contato

## Objetivo
> Este projeto tem como objetivo guardar o código fonte do site principal da empresa **System House**

## Instalação
> Para utilizar o projeto é necessário utilizar algumas ferramentas obrigatórias:
* Servidor de Aplicação PHP [***Apache***]
* Banco de dados [***MySQL ou MariaDB***]
* PHP [***Versão >= 5.5***]
* Editor de código fonte [***Visual Code, Atom, Sublime, PHP Storm, etc***]
* Browser  [***Mozilla Firefox ou Google Chrome***]
* Ferramenta de versionamento de código [***Git***]

> Há também algumas ferramentas de auxílio:
* MySQL Workbench

**OBS.: Para acelerar o desenvolvimento é altamente recomendado utilizar algum ambiente de desenvolvimento PHP tais como:**
- WAMP
- XAMPP

## Configurações
> Para conseguir utilizar o projeto é necessário fazer algumas configurações inicias:

### Configurar o banco de dados
> 1º PASSO &rarr; Criar o banco de dados.
> 2º PASSO &rarr; Criar o usuário da aplicação.
> 3º PASSO &rarr; Criar o registro inicial da empresa.

`OBSERVAÇÃO : Existe uma pasta no projeto chamada ScriptsSQL, dentro dessa pasta contém instruções SQL, basta executar esses scripts que os 3 passos acima estarão concluídos.`

## Regras para Desenvolvimento

### Git - Branchs e Commits
 - Sempre que for desenvolver uma nova funcionalidade, sempre criar uma branch nova.
 - Para garantir uma boa gerência de código, não commitar diretamente na develop, criar merge requests para aprovação e validação da nova funcionalidade.
 - Para padronização, não utilizar acentos e caracteres especiais nos nomes das branchs e nem nas mensagens dos commits.

### Código Fonte
- Sempre identar o código para garantir uma boa visualização do código e um fácil entendimento de aberturas e fechamento de tags.
- O nome dos arquivos que contém mais que uma palavra deve ser escrito em Camel Case.
- As instruções SQL devem sempre estar no topo da página antes do `DOCTYPE`, para as queries que precisam estar no corpo HTML, manter o máximo de organização no código, se possível separar em um arquivo.

### Banco de Dados
- Todos os scripts SQL de banco de dados devem ficar na pasta ** scriptsSQL ** do projeto.
- Ao criar um novo script de banco de dados, levar em consideração a numeração do último script na pasta.
- Quando for preciso alterar um script existente, analisar se o script já foi para a branch develop, se já estiver mergiado `NÃO DEVE SER ALTERADO`, deve-se criar um outro script novo com a instrução correta.
- É aconselhável manter a comunicação quanto à numeração do script para evitar a criação de um script com o mesmo nome.
- Todas as tabelas devem ser escritas com o prefixo **TB** separando as palavras com underline **(_)**, com todas as letras maiúsculas e o nome da tabela deve fazer sentido com o tipo de dado que ela irá armazenar. ***Ex.: Tabela de cadastro de usuário = TB_CAD_USUARIO***.
- Todos os campos das tabelas devem ser escritos com letras maiúsculas, separando as palavras com underline **(_)**.
