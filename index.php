<?php
  include("utils/conexao.php");
  include("utils/banco.php");
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <?php include("includes/includeHead.php"); ?>
  <?php include("includes/includeJavascript.php"); ?>
</head>

<body>
  <!--STYLE PADDING PARA FORCAR A REMOCAO DO ESPACAMENTO NO CONTAINER FLUIDO-->
  <div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">

    <?php include("includes/header.php"); ?>

  <!-- ############# Carousel ################ -->
<!--- Image Slider -->

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
	  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
	  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
	  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	</ol>
	<div class="carousel-inner">
	  <div class="carousel-item active">
		  <img class="d-block w-100" src="imgs/Slide01.jpg" alt="First slide">
      <div class="carousel-caption d-none d-md-block text-left">
        <p><h7>WebSites Responsivo</h7></p>
        <p><a class="btn btn-md btn-primary" href="#" role="button">Mais Informações</a></p>
      </div>
	  </div>
	  <div class="carousel-item">
		  <img class="d-block w-100" src="imgs/Slide02.jpg" alt="Second slide">
	  </div>
	  <div class="carousel-item">
		  <img class="d-block w-100" src="imgs/Slide03.jpg" alt="Third slide">
	  </div>
	</div>
	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
	  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	  <span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
	  <span class="carousel-control-next-icon" aria-hidden="true"></span>
	  <span class="sr-only">Next</span>
	</a>
  </div>
    <!-- ############# Carousel   ################-->




    <!-- ############# QUEM SOMOS ################-->
    <div class="jumbotron jumbotron-fluid fundo-jumbotron ">
      <div class="container">
        <h1 class="display-5">Quem somos?</h1>
        <p class="lead">
          <?php echo $descricao; ?><br />
        </p>
        <hr class="my-4">
        <p><strong>MISSÃO: </strong> <span class="text-primary"><?php echo $missao; ?></span></p>
        <p><strong>VISÃO: </strong> <span class="text-primary"><?php echo $visao; ?></span></p>
        <p class="lead">
          <a class="btn btn-primary btn-lg" href="#" role="button">Mais informações</a>
        </p>
      </div>
    </div>
    <!-- #############      QUEM SOMOS               ################-->

    <!-- #############      Conheça nossos produtos  ################-->
    <div class="row">
      <div class="col-md-12">
        <h4 class="text-center text-primary">Conheça nossos produtos</h4>
        </div>
      </div>
    </div>

    <p></p>

    <div class="row">
      <div class="col-md-4">
        <div class="card">
          <div class="card-header text-center">
            OS - House
          </div>
          <div class="card-body">
            <h4 class="card-title">Sistema de Ordem de Serviço</h4>
            <p class="card-text text-justify">
              Tenha o controle dos serviços da sua empresa com o <strong>OS - House</strong>, com ele é possível
              criar, organizar e manter as ordens de serviços de uma maneira fácil e rápida e sem burocracias.
            </p>
            <a href="#" class="btn btn-primary">Saiba mais</a>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <div class="card-header text-center">
            ImobiExpress
          </div>
          <div class="card-body">
            <h4 class="card-title">Sistema para Imobiliárias</h4>
            <p class="card-text text-justify">
              Gerencie os imóveis dos seus clientes com essa ferramenta que permite fazer upload de imagens,
              disponibilizar um imóvel e alterar as informações de uma maneira muito fácil.
            </p>
            <a href="#" class="btn btn-primary">Saiba mais</a>
          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card">
          <div class="card-header text-center">
            SoccerApp
          </div>
          <div class="card-body">
            <h4 class="card-title">Agendamento de Horários</h4>
            <p class="card-text text-justify">
              Agende o horário do futebol com os amigos, com apenas alguns cliques é possível agendar horários
              para jogar aquele futebol com os amigos e sem precisar ficar ligando.
            </p>
            <a href="#" class="btn btn-primary">Saiba mais</a>
          </div>
        </div>
      </div>
    </div>


    <br />
    <p></p>
    <br />

    
    <div class="row">
      <div class="col-md-12">
        <h3 class="text-center text-primary">Conheça nossos serviços</h2>
        </div>
      </div>
    </div>

    <br />
    <p></p>
    <br />

    <div class="row">
      <div class="col-md-6">
        <div class="card" style="width: 100%;">
          <img class="card-img-top" src="imgs/desenvolvimento.jpg" alt="Desenvolvimento de Sites">
          <div class="card-body">
            <h4 class="card-title">Desenvolvimento de Sites</h4>
            <p class="card-text">
              Se você precisa de um site para mostrar sua empresa ou um produto, você está no
              lugar certo.
            </p>
            <a href="#" class="btn btn-primary">Conheça</a>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="card" style="width: 100%;">
          <img class="card-img-top" src="imgs/consultoria.jpg" alt="Consultoria em Tecnologia">
          <div class="card-body">
            <h4 class="card-title">Consultoria em Tecnologia</h4>
            <p class="card-text">
              Sua empresa precisa de um conhecimento especializado em tecnologia? Entre em contato
              com nossa equipe.
            </p>
            <a href="#" class="btn btn-primary">Conheça</a>
          </div>
        </div>
      </div>
    </div>

    <?php include("includes/footer.php"); ?>

  </div>

  <!-- ARQUIVOS JAVASCRIPT -->
  <?php include("includes/includeJavascript.php"); ?>

</body>
</html>
