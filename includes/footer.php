<footer class="footer-class">
	<div class="footer-esquerda">
		<h3 class="text-primary">System House</h3>

		<p class="footer-links">
			<a href="index.php">Home</a>
			·
			<a href="sobre.php">Sobre</a>
			<!--·
			<a href="produtos.php">Produtos</a>
			·
			<a href="clientes.php">Clientes</a-->
			·
			<a href="contato.php">Contato</a>
		</p>
		<p class="footer-empresa">System House &copy; 2017</p>
	</div>

	<div class="footer-center">
		<div>
			<i class="fa fa-map-marker"></i>
			<p><span><?php echo $logradouro ?></span> Minas Gerais, Araguari</p>
		</div>

		<div>
			<i class="fa fa-phone"></i>
				<p><span>
					<?php echo $celular_1; ?></br>
					<?php echo $celular_2; ?></br>
					<?php echo $celular_3; ?></br>
				</p></span>
		</div>

		<div>
			<i class="fa fa-envelope"></i>
			<p><a href="mailto:suporte@systemhouse.com.br">
				<?php echo $email; ?>
			</a></p>
		</div>
	</div>

	<div class="footer-direita">
		<p class="footer-sobre">
			<span>Sobre a empresa</span>
			System House é uma empresa que oferece produtos e serviços de tecnologia para sua empresa.
      Conheça a System House, entre em contato através do link de contato.
		</p>

		<div class="footer-icones">
			<a href="https://www.facebook.com/SystemHouseFace/"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-linkedin"></i></a>
			<a href="#"><i class="fa fa-github"></i></a>
		</div>
	</div>
</footer>
