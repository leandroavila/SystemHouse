<!DOCTYPE html>
<html>
<head>
  <?php include("../includes/includeHead.php"); ?>
</head>

<body>
  <!--STYLE PADDING PARA FORCAR A REMOCAO DO ESPACAMENTO NO CONTAINER FLUIDO-->
  <div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">

    <div class="row jumbotron jumbotron-fluid small">
      <div class="col-md-12">

        <?php

          //TESTE DE CONEXAO
          include_once("conexao.php");

          /*
          EXISTE DOIS TIPOS DE OBTER OS DADOS:
          -> QUERY STATEMENT = QUERY SIMPLES COM SELECT E FROM
          -> PREPARED STATEMENTS = MAIS SEGURO E PASSAGEM DE PARAMETROS ATRAVES DO BIND
          */

          //-----QUERY STATEMENT -----

          //OBTER A CONEXAO ATRAVES DA FUNCAO
          $conexao = getConnection();

          //QUERY PARA EXECUCAO SEM CLAUSULA WHERE
          $statement = $conexao->query( 'SELECT * FROM TB_EMPRESA' );

          //OBTENDO OS DADOS APARTIR DE UMA QUERY simplexml_load_string
          //FETCH ALL - EXECUTA A QUERY E PEGA TODOS OS RESULTADOS
          //PDO::FETCH ALL - ELIMINA RESULTADOS QUE NAO POSSUEM UM NOME DE COLUNA
          $resultStatement = $statement->fetchAll(PDO::FETCH_ASSOC);

          //MOSTRAR OS DADOS QUE FORAM OBTIDOS, ITERANDO SOBRE O RESULTADO
          echo "<pre>";
          foreach ($resultStatement as $resultTemp) {
            print_r($resultTemp);
          }
          echo "</pre>";

          //FECHANDO A CONEXAO
          $conexao = NULL;

          //CRIANDO UMA NOVA CONEXAO PARA O EXEMPLO DE PREPARED STATEMENT
          //OBTENDO A CONEXAO ATRAVES DA FUNCAO
          $conexao2 = getConnection();

          //CRIANDO UM PREPARED STATEMENT PREPARANDO A CONSULTA PARA RECEBER OS PARAMETROS
          //:ID - INDICA QUE ALI SERA PASSADO O VALOR DE UM PARAMETRO
          $preparedStatement = $conexao2->prepare('SELECT * FROM TB_EMPRESA WHERE ID = :id');

          //CRIANDO UM PARAMETRO TEMPORARIO
          //OBS.: O NOME DO PARAMETRO DEVE SER IGUAL AO NOME DA VARIAVEL
          //CASO CONTRARIO SERA LANCADO UM ERRO NA TELA
          $id = 1;

          //FAZENDO O BIND NO VALOR DO PARAMETRO QUE EH ESPERADO NA QUERY
          //DEVE SEGUIR A SEQUENCIA DE PARAMETROS INFORMADOS NA ORDEM
          $preparedStatement->bindParam(':id', $id);

          //EXECUTANDO A QUERY
          $preparedStatement->execute();

          //MOSTRAR OS DADOS OBTIDOS NA EXECUCAO DA CONSULTA
          //$preparedStatement->fetch(PDO::FETCH_ASSOC) - RETORNA UM RESULTADO POR VEZ E ATRIBUI A VARIAVEL $row
          //ENQUANTO A VARIAVEL $row RECEBER UM RESULTADO, SERA MOSTRADO O VALOR DA LINHA DO RESULTADO
          echo "<hr><pre>";
          while($resultPrepStatement = $preparedStatement->fetch(PDO::FETCH_ASSOC)) {
            print_r($resultPrepStatement);
          }
          echo "</pre>";

          //FECHANDO A CONEXAO
          $conexao2 = NULL;

        ?>

      </div>
    </div>

  </div>

  <!-- ARQUIVOS JAVASCRIPT -->
  <?php include("../includes/includeJavascript.php"); ?>

</body>
</html>
