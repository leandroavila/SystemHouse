<?php
  //Variáveis para armazenar os valores para envio do email
  $nome = $_POST['nome'];
  $email = $_POST['email'];
  $telefone = $_POST['telefone'];
  $assunto = $_POST['assunto'];
  $mensagem = $_POST['mensagem'];
  $data_envio = date('d/m/Y');
  $hora_envio = date('H:i:s');

  //Inclui o arquivo phpmailer.php localizado na pasta phpmailer
  require_once("../phpmailer/PHPMailer.php");
  require_once("../phpmailer/SMTP.php");

  //Inicia a classe PHPMailer
  $mail = new \PHPMailer\PHPMailer\PHPMailer();

  //Define os dados do servidor e tipo de conexão
  $mail->IsSMTP(); //Define que a mensagem será SMTP
  $mail->Host = "smtp.gmail.com"; //Endereço do servidor SMTP
  $mail->Port = 465; //Porta TCP para a conexão
  $mail->SMTPAutoTLS = false; //Utiliza TLS Automaticamente se disponível
  $mail->SMTPAuth = true; //Usar autenticação SMTP - Sim
  $mail->SMTPSecure = 'ssl';
  $mail->Username = 'systemhousearaguari@gmail.com'; //Usuário de e-mail
  $mail->Password = 'house2017'; //Senha do usuário de e-mail

  //Dados do Remetente
  $mail->From = "systemhousearaguari@gmail.com"; //Seu e-mail
  $mail->FromName = "System House"; //Seu nome

  //Dados do(s) Destinatário(s)
  $mail->AddAddress('systemhousearaguari@gmail.com', 'System House'); //Os campos podem ser substituidos por variáveis
  //$mail->AddAddress('webmaster@nomedoseudominio.com'); //Caso queira receber uma copia
  //$mail->AddCC('ciclano@site.net', 'Ciclano'); //Copia
  //$mail->AddBCC('eduardocostadiniz67@gmail.com', 'Eduardo Costa'); //Cópia Oculta
  //$mail->AddBCC('leferav@gmail.com', 'Leandro Avila'); //Cópia Oculta

  //Dados Técnicos da Mensagem
  $mail->IsHTML(true); //Define que o e-mail será enviado como HTML
  $mail->CharSet = 'UTF-8'; //Charset da mensagem (opcional)

  //Define a mensagem (Texto e Assunto)
  $mail->Subject = $assunto; //Assunto da mensagem

  $mail->Body = "Contato de: {$nome}<br><br>E-mail de contato: {$email}<br><br>Telefone de: {$telefone}<br><br>Assunto: {$assunto}<br><br><hr>Mensagem: <pre>{$mensagem}</pre><hr>Data de Envio: {$data_envio}<br><br>Hora de Envio: {$hora_envio}"; //Corpo do e-mail

  //$mail->AltBody = "Este é o corpo da mensagem de teste, somente Texto! \r\n :)";

  //Define os anexos (opcional)
  //$mail->AddAttachment("/opt/lampp/htdocs/site-principal/imgs", "responsivo.png"); //Insere um anexo

  //Envia o e-mail
  $enviado = $mail->Send();

  //Limpa os destinatários e os anexos
  $mail->ClearAllRecipients();
  $mail->ClearAttachments();

  //Exibe uma mensagem de resultado
  if ($enviado) {
    header("location:../contato.php?status=y");
  } else {
    echo "Não foi possível enviar o e-mail.";
    echo "<b>Informações do erro:</b> " . $mail->ErrorInfo;
  }
?>
