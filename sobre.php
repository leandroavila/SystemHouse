<?php
  include("utils/conexao.php");
  include("utils/banco.php");
?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
  <?php include("includes/includeHead.php"); ?>
</head>

<body>
  <!--STYLE PADDING PARA FORCAR A REMOCAO DO ESPACAMENTO NO CONTAINER FLUIDO-->
  <div class="container-fluid" style="padding-left: 0px; padding-right: 0px;">

    <?php include("includes/header.php"); ?>

    <div class="row margin-fixada">
      <!-- CONTEUDO DA PAGINAA -->
      <div class="col-sm-8 sobre_nos" >
        <b><h5>Sobre Nós</h5></b>
        <a>Uma empresa com pessoas dedicadas e eficientes, dispostas atendê-lo a fim de alcançar o seu sucesso online.</a>
      </div>
      <div class="col-sm-12 sobre-img">
         <img src="imgs/sobrenos.png"  class="img-fluid margin-fluid" />
      </div>
      <div class="col-sm-4 sobre_cont" >
        <b><h5>Visão</h5></b>
        <a><?php echo $visao; ?></a>
      </div>
      <div class="col-sm-4 sobre_cont" >
        <b><h5>Missão</h5></b>
        <a><?php echo $missao; ?></a>
      </div>
      <div class="col-sm-4 sobre_cont" >
        <b><h5>Valores</h5></b>
        <a><?php echo $valores; ?></a>
      </div>
    </div>
    <?php include("includes/footer.php"); ?>
  </div>

  <!-- ARQUIVOS JAVASCRIPT -->
  <?php include("includes/includeJavascript.php"); ?>

</body>
</html>
